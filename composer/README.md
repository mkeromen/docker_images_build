Composer
-------------
To start a [Composer](https://getcomposer.org/) container : 

1/ Create a data container
`docker run -d -v /tmp:/var/www --name data debian:wheezy`

2/ Launch Composer container. Here I initialize a new Symfony 2 project.
`docker run --rm --volumes-from data --name sf2-init kmelkez/composer create-project symfony/framework-standard-edition website`
