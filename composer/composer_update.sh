#!/bin/bash

docker build -t kmelkez/composer /usr/local/bin/docker_build/sharooms/composer
docker run --rm --volumes-from website_data_1 --name composer-update --entrypoint=/bin/bash kmelkez/composer -c 'cd /var/www && composer update '$1
