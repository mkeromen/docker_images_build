#!/bin/bash

DATE_NOW=$(date +%F_%H-%M-%S)
DUMPED_FILE=$LOCATION_DIR/${FILE_PREFIX}_${DATE_NOW}.sql

if [ $ACTION = 'dump' ]; then
    mkdir -p $LOCATION_DIR
    mysqldump --no-data -h db -u root -p$DB_ENV_MYSQL_ROOT_PASSWORD $DB_ENV_MYSQL_DATABASE > $DUMPED_FILE
    mysqldump --no-create-info -h db -u root -p$DB_ENV_MYSQL_ROOT_PASSWORD $DB_ENV_MYSQL_DATABASE $CONTENT_TABLES >> $DUMPED_FILE
elif [ $ACTION = 'import' ]; then
    cat $(ls -t ${LOCATION_DIR}/${FILE_PREFIX}_*.sql | head -n1) | mysql -h db -u root -p$DB_ENV_MYSQL_ROOT_PASSWORD $DB_ENV_MYSQL_DATABASE
fi

