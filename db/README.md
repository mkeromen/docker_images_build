MySQL client
-------------
Very lightweight MySQL client image. I'm using it to interact with a MySQL Server.
You have two simple options to interact with your database : `dump` (mysqldump with -d option) and `import` (mysql command line)

`docker run --rm --name mysql_migration -e ACTION=dump -e FILE_PREFIX="mysql" -e LOCATION_DIR="/var/www/deploy/db" --link $MYSQL_CONTAINER_NAME:db --volumes-from $DATA_CONTAINER_NAME kmelkez/mysql_client
`
>  It's important to name your MySQL container alias `db` because i'm using it to get linked environment variables of the MySQL container. [Here](https://docs.docker.com/userguide/dockerlinks/#environment-variables) is the convention for inherited environment variables.

--
> For linked MySQL container, I'm using the official [MySQL repository](https://registry.hub.docker.com/_/mysql/)  
