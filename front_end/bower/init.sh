#!/bin/bash

#PUBLIC_D="/var/www/src/Sharooms/AppBundle/Resources/public"
if [ "$USER" != "root" ]; then
    adduser --disabled-password --gecos "" $USER
fi

cd $WORKSPACE && \
rm -rf vendor/* && \
bower install --allow-root && \
chown -R $USER:$USER ../.
