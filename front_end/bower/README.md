Bower tool
-------------
Start a bower container to initiate a project with all your front-end dependencies. 

`docker run -d --name bower -e WORKSPACE=/var/www/yourWorkspace -e USER=yourHostUserName -v /var/www:/var/www kmelkez/bower
`
>  Be careful when you pass your user in USER environment variable. It can only work if your host uid equal your container user uid.
Take a look [here](http://stackoverflow.com/questions/23544282/what-is-the-best-way-to-manage-permissions-for-docker-shared-volumes)
