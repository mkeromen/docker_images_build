Gulp tool
-------------
Start a [gulp](http://gulpjs.com/) container to run a project with all your front-end tools based on gulp tasks. 

`docker run -d --name gulp -e WORKSPACE=/var/www/yourWorkspace -e USER=yourHostUserName -e MODULES=event-stream gulp-less gulp-include -e TASKS=myTask1 myTask2 -v /var/www:/var/www kmelkez/gulp
`
>  If you want to build a bower container to manage your dependencies, take a look [kmelkez/bower](https://registry.hub.docker.com/u/kmelkez/bower)
