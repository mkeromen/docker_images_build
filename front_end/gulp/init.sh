#!/bin/bash

cd $WORKSPACE && \ 
npm install gulp --save-dev && \
npm install $MODULES

if [ "$USER" != "root" ]; then
    adduser --disabled-password --gecos "" $USER
    su - $USER
fi

TZ=$TIMEZONE gulp $TASKS
