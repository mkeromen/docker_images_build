#!/bin/bash
for file in $(find /etc/nginx/sites-available -maxdepth 1 -type f -printf "%f\n")
  do
    ln -s /etc/nginx/sites-available/$file /etc/nginx/sites-enabled/$file
done

/usr/sbin/nginx
