Nginx server
-------------

Lightweight Nginx image for running a non-daemonized container (process in foreground). 
The simple way to configure this container is to mount a volume (`your/nginx/configuration/path` to `/etc/nginx`).

`docker run -d -v /your/nginx/configuration/path:/etc/nginx kmelkez/nginx`

Symlink in `sites-enabled` folder are automatically created based on your `sites-available` content.

>  If you want to build a PHP container too, take a look [kmelkez/php-fpm5.6](https://registry.hub.docker.com/u/kmelkez/php-fpm5.6)
