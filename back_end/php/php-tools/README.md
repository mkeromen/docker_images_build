PHP tools
-------------
Start a container inherited from [kmelkez/composer](https://registry.hub.docker.com/u/kmelkez/composer/). This image contains tools for php development or continuous integration like xdebug, phpunit.

`docker run -it --name php-tools kmelkez/php-tools`

Once it is created, you can start it and attach it for build your development environment very quickly

`docker start php-tools`
`docker attach php-tools`
