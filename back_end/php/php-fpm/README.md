PHP-FPM 5.6
-------------

Simple PHP 5.6 image running with php-fpm and some useful extensions (cli, gd, locales, etc).
PHP daemon is configured to listen on port 9000 and container is exposed on the same port. 
Thus, you can separate your server configuration (Nginx in my case) and run it in another container.

>  If you want to build a Nginx container too. take a look [kmelkez/nginx](https://registry.hub.docker.com/u/kmelkez/nginx)
