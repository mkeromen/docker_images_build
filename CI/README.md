Jenkins
-------------
To start a [Jenkins](https://jenkins-ci.org/) container : 

`docker run -d --name jenkins -e "PLUGINS=plugin1 plugin2" -p 8080:8080 kmelkez/jenkins`
